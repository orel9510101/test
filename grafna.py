from prometheus_client import Gauge
from prometheus_client import start_http_server, Info
import cpuinfo
import psutil
from grafana_api.grafana_face import GrafanaFace

g_ram = Gauge('RAM_Description', 'Description of ram')
i = Info('my_build_version', 'Description of cpu')


def process_request():

    ram_use = int(psutil.virtual_memory()[2])
    g_ram.set(ram_use)

    cpu_info = cpuinfo.get_cpu_info()["brand_raw"]
    i.info({'cpu info': cpu_info})


if __name__ == '__main__':
    start_http_server(8000)
    while True:
        process_request()
